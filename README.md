# Unity Cloud Build Helper

## Introduction

Helper script for managing cloud builds of Unity projects using [Unity Cloud Build](https://unity3d.com/unity/features/cloud-build). Intended for use with [Team City](https://www.jetbrains.com/teamcity/) builds.

## Further Info

See Unity Cloud Build REST API documentation [here](https://build-api.cloud.unity3d.com/docs/1.0.0/index.html).

## Setup

* `pip install -r requirements.txt`
  * (Run this inside the appropriate Python venv, in PyCharm this can be done via the Terminal tap inside PyCharm).


## Usage

Example build:

`./run_cloud_build.py --api-key <Unity API Key> --org-id cape-guy --project-id flames_unityproj --build-target-id teamcity-macos-universal --commit 1432d201cfb91d8e3cfcff58c9c19363e19507f0 --build-label Example --artifact-download-path ./artifacts --delete_cloud_artifacts`

Example continue using existing build id:

`./run_cloud_build.py --continue-build-id <Build ID to Continue> --api-key <Unity API Key> --org-id cape-guy --project-id flames_unityproj --build-target-id teamcity-macos-universal --commit 1432d201cfb91d8e3cfcff58c9c19363e19507f0 --build-label Example --artifact-download-path ./artifacts --delete-cloud-artifacts`
