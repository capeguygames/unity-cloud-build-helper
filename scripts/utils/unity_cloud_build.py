# See https://realpython.com/api-integration-in-python/
# pylint: disable=C0330

import json
from pprint import pprint
import urllib3
import sys


class CloudAPIException(Exception):
	pass


class UnityCloudBuild:
	"""
	Handles Unity Cloud Build REST API Calls.
	See https://build-api.cloud.unity3d.com/docs/1.0.0/index.html
	"""

	def __init__(self, org_id:str, api_key:str):
		self._org_id = org_id
		self._api_key = api_key
		self._api_url = 'https://build-api.cloud.unity3d.com/api/v1'

		retries = urllib3.util.Retry(connect=5, read=2, redirect=5)
		self._http = urllib3.PoolManager(retries=retries)
		self._headers = {'Authorization': 'Basic {API_KEY}'.format(API_KEY=self._api_key)}

	def org_api_url(self, ):
		return '{API_URL}/orgs/{ORG_ID}'.format(
			API_URL=self._api_url,
			ORG_ID=self._org_id)

	def project_api_url(self, project_id):
		return '{ORG_URL}/projects/{PROJECT_ID}'.format(
			ORG_URL=self.org_api_url(),
			PROJECT_ID=project_id)

	@staticmethod
	def _combine_headers_static(base_headers, headers):
		combined_headers = base_headers.copy()
		if headers is not None:
			combined_headers.update(headers)
		return combined_headers

	def _combine_headers(self, headers):
		return UnityCloudBuild._combine_headers_static(self._headers, headers)

	def cloud_req(self, type, req, body_dict=None, preload_content=False, verbose=False):
		headers = {}
		body = None
		if body_dict is not None:
			headers['Content-Type'] = 'application/json'
			body = json.dumps(body_dict)
		headers = self._combine_headers(headers)

		response = self._http.request(type, req, headers=headers, body=body, preload_content=preload_content)
		if verbose:
			print('Cloud ' + type + ': ' + req)
			if body_dict is not None:
				print('Body:')
				print(str(body_dict))
			print('Response:')
			pprint(vars(response))
			sys.stdout.flush()
		return response

	def cloud_get(self, req, body_dict=None, preload_content=False, verbose=False):
		return self.cloud_req(
			'GET', req, body_dict=body_dict, preload_content=preload_content, verbose=verbose)

	def cloud_post(self, req, body_dict=None, preload_content=False, verbose=False):
		return self.cloud_req(
			'POST', req, body_dict=body_dict, preload_content=preload_content, verbose=verbose)

	def cloud_delete(self, req, body_dict=None, preload_content=False, verbose=False):
		return self.cloud_req(
			'DELETE', req, body_dict=body_dict, preload_content=preload_content, verbose=verbose)

	@staticmethod
	def check_resp(method_name, req, resp, expected_status):
		if hasattr(resp, 'error') or resp.status != expected_status:
			raise CloudAPIException("Failed {NAME}: {REQ}: {RESP} - {DATA}".format(
				NAME=method_name, REQ=req, RESP=str(resp.status), DATA=json.loads(resp.data.decode('utf-8'))))

	def start_build(self, project_id, build_target_id, commit_hash = None, build_label=None, clean_build=False, verbose=False):
		req = self.project_api_url(project_id) + \
		      '/buildtargets/{BUILD_TARGET_ID}/builds'.format(BUILD_TARGET_ID=build_target_id)
		body_dict = {
			"clean": clean_build,
			"delay": 0,
			"headless": False,
		}

		if commit_hash is not None:
			body_dict["commit"] = commit_hash

		if build_label is not None:
			body_dict["label"] = build_label

		resp = self.cloud_post(req, body_dict=body_dict, verbose=verbose)
		UnityCloudBuild.check_resp('start_build', req, resp, 202)
		return json.loads(resp.data.decode('utf-8'))[-1]

	def get_build_status(self, project_id, build_target_id, build_id, verbose=False):
		req = self.project_api_url(project_id) + \
		      '/buildtargets/{BUILD_TARGET_ID}/builds/{BUILD_ID}'.format( \
		      	BUILD_TARGET_ID=build_target_id, BUILD_ID=build_id)

		resp = self.cloud_get(req, verbose=verbose)
		UnityCloudBuild.check_resp('get_build_status', req, resp, 200)
		return json.loads(resp.data.decode('utf-8'))

	def cancel_build(self, project_id, build_target_id, build_id, verbose=False):
		req = self.project_api_url(project_id) + \
			'/buildtargets/{BUILD_TARGET_ID}/builds/{BUILD_ID}'.format( \
				BUILD_TARGET_ID=build_target_id, BUILD_ID=build_id)

		resp = self.cloud_delete(req, verbose=verbose)
		UnityCloudBuild.check_resp('cancel_build', req, resp, 204)

	def delete_build_artifacts(self, project_id, build_target_id, build_id, verbose=False):
		req = self.project_api_url(project_id) + \
		      '/buildtargets/{BUILD_TARGET_ID}/builds/{BUILD_ID}/artifacts'.format( \
		      	BUILD_TARGET_ID=build_target_id, BUILD_ID=build_id)

		resp = self.cloud_delete(req, verbose=verbose)
		UnityCloudBuild.check_resp('delete_build_artifacts', req, resp, 200)


	@staticmethod
	def stream_response(resp, output_file_path, print_progress=False):
		if print_progress:
			bytes_read_so_far = 0
			bytes_per_dot = 512 * 1024
			dots_printed = 0

		print('.', end='')
		with open(output_file_path, 'wb') as file:
			for chunk in resp.stream():
				file.write(chunk)
				if print_progress:
					bytes_read_so_far = bytes_read_so_far + len(chunk)
					dots_desired = bytes_read_so_far / bytes_per_dot
					while dots_printed < dots_desired:
						print('.', end='')
						dots_printed = dots_printed + 1

		resp.release_conn()
		if print_progress:
			print('Done')

	def get_build_log(self, project_id, build_target_id, build_id, log_file_path, compact=False, print_progress=False, verbose=False):
		req = self.project_api_url(project_id) + \
		      '/buildtargets/{BUILD_TARGET_ID}/builds/{BUILD_ID}/log?compact={COMPACT}'.format( \
		      	BUILD_TARGET_ID=build_target_id, BUILD_ID=build_id, COMPACT=str(compact).lower())
		resp = self.cloud_get(req, preload_content=False, verbose=verbose)
		UnityCloudBuild.check_resp('cancel_build', req, resp, 200)
		UnityCloudBuild.stream_response(resp, log_file_path, print_progress)
