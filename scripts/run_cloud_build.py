#!/usr/bin/env python3

# pylint: disable=C0330

from datetime import datetime
import os
import signal
import sys
import time

import utils.unity_cloud_build as cloud_build

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))


def arg_parser():
	import argparse
	parser = argparse.ArgumentParser(description='Unity Cloud Build Helper.')
	parser.add_argument('--api-key', '-k', type=str,
						help='The API Key for Unity Cloud build')
	parser.add_argument('--build-label', '-bl', type=str,
						help='The label for the new build')
	parser.add_argument('--artifact-download-path', '-ap', type=str,
						help='The path of the folder to download the artifacts to')
	parser.add_argument('--delete-cloud-artifacts', '-ad', action='store_true',
						help='If the cloud artifacts should be deleted (after they have been download if requested)')
	parser.add_argument('--commit', '-c', type=str,
						help='The commit hash for the build')
	parser.add_argument('--clean-build',
						action='store_true',
						default=False,
						help='If the build should be a clean build')
	parser.add_argument('--auto-clean-build',
						action='store_true',
						default=False,
						help='Performs a clean build if incremental_build_token.txt is not present')
	parser.add_argument('--verbose',
						action='store_true',
						default=False,
						help='Print out extra information about calls to the server etc.')
	parser.add_argument('--continue-build-id',
						type=int,
						default=None,
						help='Continue an existing build (with the given id) rather than starting sa new one.')
	required_named = parser.add_argument_group('required named arguments')
	required_named.add_argument('--org-id', '-o', type=str,
								help='The Organization Id for Unity Cloud build')
	required_named.add_argument('--project-id', '-p', type=str,
								help='The Project Id for Unity Cloud build')
	required_named.add_argument('--build-target-id', '-b', type=str,
								help='The Build Target Id for Unity Cloud build')
	return parser


def do_cloud_build(api_key,
	               org_id,
	               project_id,
	               build_target_id,
	               commit_hash,
	               build_label,
	               artifact_download_path,
	               delete_cloud_artifacts,
	               clean_build=False,
				   continue_build_id=None,
				   verbose=False):

	cloud = cloud_build.UnityCloudBuild(org_id, api_key)

	if continue_build_id is None:
		build_resp = do_start_cloud_build(
			cloud, org_id, project_id, build_target_id, commit_hash, build_label, clean_build, verbose=verbose)
		if 'build' in build_resp:
			build_id = int(build_resp['build'])
		else:
			raise cloud_build.CloudAPIException(build_resp['error'])
		build_status = build_resp['buildStatus']
	else:
		build_id = continue_build_id
		build_status_dict = cloud.get_build_status(project_id, build_target_id, build_id, verbose=verbose)
		build_status = build_status_dict['buildStatus']

	# Register SIGINT handler
	original_sigint_handler = signal.getsignal(signal.SIGINT)

	def signal_handler(sig, frame):
		if delete_cloud_artifacts and build_id is not None:
			print("Build Interrupted. Deleting cloud build.")
			cloud.cancel_build(project_id, build_target_id, build_id, verbose=verbose)
			try:
				do_delete_cloud_artifacts(cloud, project_id, build_target_id, build_id, verbose=verbose)
			except cloud_build.CloudAPIException:
				# Just ignore failures here.
				pass
			sys.exit(0)
	signal.signal(signal.SIGINT, signal_handler)

	(status_resp, build_success) = do_wait_for_build(cloud, project_id, build_target_id, build_id, build_status, verbose=verbose)

	print('\n---\n')
	sys.stdout.flush()

	do_download_artifacts(cloud, project_id, build_target_id, build_id, artifact_download_path, status_resp, verbose=verbose)

	if delete_cloud_artifacts:
		do_delete_cloud_artifacts(cloud, project_id, build_target_id, build_id, verbose=verbose)

	# Restore SIGINT handler
	signal.signal(signal.SIGINT, original_sigint_handler)

	print('Build ' + ('successful :-)' if build_success else 'failed :-('))
	sys.stdout.flush()

	return build_success


def do_start_cloud_build(cloud, org_id, project_id, build_target_id, commit_hash, build_label, clean_build=False, verbose=False):
	try:
		build_resp = cloud.start_build(project_id, build_target_id, commit_hash, build_label, clean_build, verbose=verbose)
	except cloud_build.CloudAPIException as exc:
		print("Error starting build: " + str(exc))
		return False

	try:
		build_id = build_resp['build']
		print("Started cloud build")
		print("Build Id: " + str(build_id))
		print("Organization Id: " + org_id)
		print("Project Id: " + project_id)
		print("Build Target Id: " + build_target_id)
		print("Clean Build: " + str(clean_build))
		if commit_hash is not None:
			print("Commit Hash: " + commit_hash)
		if build_label is not None:
			print("Build Label: " + build_label)
		print("Verbose: " + str(verbose))

		print("See build info and logs at:\n\nhttps://developer.cloud.unity3d.com/build/orgs/{ORG_ID}/projects/{PROJECT_ID}/buildtargets/{BUILD_TARGET_ID}/builds/{BUILD_ID}/log/\n".format( \
			ORG_ID=org_id, PROJECT_ID=project_id, BUILD_TARGET_ID=build_target_id, BUILD_ID=str(build_id)))
	except:
		print("Exception Printing Build Info: " + str(build_resp))

	sys.stdout.flush()

	return build_resp


def do_wait_for_build(cloud, project_id, build_target_id, build_id, build_status, verbose=False):
	print("Cloud Build status is: " + build_status)

	unfinished_statuses = ['queued', 'sentToBuilder', 'started', 'restarted']

	status_resp = None
	while status_resp is None or build_status in unfinished_statuses:
		if status_resp is not None:
			time.sleep(20)
		# print('.', end='')
		status_resp = cloud.get_build_status(project_id, build_target_id, build_id, verbose=verbose)
		new_build_status = status_resp['buildStatus']
		if new_build_status != build_status:
			# print('')
			print("Cloud Build status changed to: " + new_build_status)
		build_status = new_build_status
		sys.stdout.flush()

	build_success = build_status == 'success'

	print('Cloud Build finished')
	sys.stdout.flush()

	return status_resp, build_success


def do_download_artifacts(cloud, project_id, build_target_id, build_id, artifact_download_path, status_resp, verbose=False):
	if artifact_download_path is not None:
		print("Downloading build artifacts:")
		link_data = status_resp['links']

		compact_log_file_name = "build.compact.log"
		print('Downloading ' + compact_log_file_name + ':', end='')
		sys.stdout.flush()
		compact_log_file_path = os.path.join(artifact_download_path, compact_log_file_name)
		make_dir_if_necessary(compact_log_file_path)
		cloud.get_build_log(project_id, build_target_id, build_id, compact_log_file_path, compact=True, print_progress=True, verbose=verbose)

		log_file_name = "build.log"
		print('Downloading ' + log_file_name + ':', end='')
		sys.stdout.flush()
		log_file_path = os.path.join(artifact_download_path, log_file_name)
		make_dir_if_necessary(log_file_path)
		cloud.get_build_log(project_id, build_target_id, build_id, log_file_path, print_progress=True, verbose=verbose)

		artifacts_data = link_data['artifacts']

		for artifact_data in artifacts_data:
			for file_data in artifact_data['files']:
				file_name = file_data['filename']
				file_href = file_data['href']
				print('Downloading ' + file_name + ':', end='')
				sys.stdout.flush()

				resp = cloud.cloud_get(file_href, preload_content=False, verbose=verbose)
				cloud_build.UnityCloudBuild.check_resp('Download ' + file_name, file_href, resp, 200)

				write_file_path = os.path.join(artifact_download_path, file_name)
				make_dir_if_necessary(write_file_path)
				cloud_build.UnityCloudBuild.stream_response(resp, write_file_path, print_progress=True)

		print("Finished downloading artifacts")
		print("== Begin Compact Log ==")
		sys.stdout.flush()
		with open(compact_log_file_path) as log_file:
			for log_line in log_file.readlines():
				print(log_line.strip())
		print("== End Compact Log ==")
		sys.stdout.flush()


def make_dir_if_necessary(file_path):
	dir_path = os.path.dirname(file_path)
	if not os.path.isdir(dir_path):
		os.makedirs(dir_path)


def do_delete_cloud_artifacts(cloud, project_id, build_target_id, build_id, verbose=False):
	print("Clearing Artifacts From Cloud Build")
	sys.stdout.flush()
	cloud.delete_build_artifacts(project_id, build_target_id, build_id, verbose=verbose)
	print("Done")
	sys.stdout.flush()


def main():
	args = arg_parser().parse_args()

	if args.org_id is None:
		print("Organization Id is required.")
		return False

	if args.project_id is None:
		print("Project Id is required.")
		return False

	if args.build_target_id is None:
		print("Build Target Id is required.")
		return False

	if args.api_key is None:
		# Try the UNITY_CLOUD_BUILD_API_KEY environment variable
		if 'UNITY_CLOUD_BUILD_API_KEY' in os.environ:
			args.api_key = os.environ['UNITY_CLOUD_BUILD_API_KEY']
		else:
			import getpass
			args.api_key = getpass.getpass()

	# Decide if it should be a clean build...
	clean_build = args.clean_build
	if args.auto_clean_build:
		token_file_path = 'incremental_build_token.txt'
		if not os.path.isfile(token_file_path):
			with open(token_file_path, 'w') as file:
				file.write("Previous build at: " + str(datetime.now()))
			clean_build = True

	return do_cloud_build(
		args.api_key,
		args.org_id,
		args.project_id,
		args.build_target_id,
		args.commit,
		args.build_label,
		args.artifact_download_path,
		args.delete_cloud_artifacts,
		clean_build,
		args.continue_build_id,
		args.verbose)


if __name__ == '__main__':
	sys.exit(0 if main() else 1)
